<?php

class User_Model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->library('session');
	}

	public function get_user_by_username($username)
	{
		return $this->db->get_where('users', array('username' => $username))->row();
	}

	public function authenticate($username, $password)
	{
		if (!$this->get_user_by_username($username)) {
			return false;
		} else {
			$result = $this->get_user_by_username($username);
			if ($result->password == $password) {
				return true;
			}
			return false;
		}
	}

	public function register($username, $password)
	{
		if($this->get_user_by_username($username))
		{
			return false;
		} else {
			$data = array(
        		'username' => $username,
        		'password' => $password
			);
			$this->db->insert('users', $data);
			return true;
		}
	}
}
