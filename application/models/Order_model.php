<?php

class Order_Model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function makeOrder($data)
	{
		return $this->db->insert('orders', $data);
	}

	public function get_all_orders($user_id)
	{
		return $this->db->get_where('orders', ['user_id' => $user_id])->result();
	}
}
