<?php

class Product_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function makeOrder($data)
	{
		$product_id = $data['id'];
		unset($data['id']);
		$this->db->where(['id' => $product_id]);
		$this->db->update('products', $data);
	}

	public function get_all_products()
	{
		return $this->db->get('products')->result();
	}

	public function get_product_by_id($product_id)
	{
		return $this->db->get_where('products', ['id' => $product_id])->row();
	}

	public function get_product_by_name($product_name)
	{
		return $this->db->get_where('products', ['product_name' => $product_name])->row();
	}
}