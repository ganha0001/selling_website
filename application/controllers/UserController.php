<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper('url');
		$this->load->helper('common');
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function actionLogin()
	{
		$post = $this->input->post();
		if ($this->user_model->authenticate($post['username'], $post['password']))
		{
			$this->session->set_userdata([
				'username' => $post['username'],
				'isLoggedIn' => true
			]);
			redirect(base_url());
		} else 
		{
			redirect(base_url() . "user/login");
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('isLoggedIn');
		session_destroy();
		redirect(base_url());
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function actionRegister()
	{
		$post = $this->input->post();
		if ($this->user_model->register($post['username'], $post['password'])) {
			$this->session->set_userdata([
				'username' => $username,
				'isLoggedIn' => true
			]);
			redirect(base_url());
		} else {
			redirect(base_url() . "user/register");
		}
	}
}