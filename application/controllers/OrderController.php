<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OrderController extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('user_model');
		$this->load->model('order_model');
		$this->load->helper('url');
		$this->load->helper('common');
		$this->load->library('session');
		if (!check_logged()) redirect(base_url() . 'user/login');
	}

	public function actionOrder() 
	{
		$product_id = $this->input->post('product_id');
		$data['product'] = $this->product_model->get_product_by_id($product_id);

		$this->load->view('order', $data);
	}

	public function confirmOrder()
	{
		$post = $this->input->post();
		$product = $this->product_model->get_product_by_name($post['productName']);
		$user = $this->user_model->get_user_by_username($this->session->userdata('username'));
		if ($product->instock < $post['quantity'])
		{ 
			echo 'Not enough in number!';
			return false;
		}
		$data = [
			'product_id' => $product->id,
			'user_id' => $user->id,
			'quantity' => $post['quantity'],
			'description' => 'Customer name: '. $post['name'] .'   Customer address: ' . $post['address']
		];
		$this->order_model->makeOrder($data);

		$data = [
			'id' => $product->id,
			'instock' => ($product->instock - $post['quantity'])
		];
		$this->product_model->makeOrder($data);
		echo "We've received your order!";
	}

	public function history()
	{
		$user = $this->user_model->get_user_by_username($this->session->userdata('username'));
		if(!$this->order_model->get_all_orders($user->id))
		{ 
			echo 'No order!';
		} else 
		{
			$data['orders'] = $this->order_model->get_all_orders($user->id);
			$this->load->view('history', $data);
		}
	}
}