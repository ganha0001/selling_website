<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductController extends CI_Controller 
{

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('product_model');
		$this->load->model('user_model');
		$this->load->helper('common');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['products'] = $this->product_model->get_all_products();
		$this->load->view('homepage', $data);
	}

	public function search()
	{
		if (!check_logged()) 
		{
			return redirect(base_url());
		}
		$this->load->view('search');
	}

	public function actionSearch()
	{
		$product_name = $this->input->post('searchitem');
		if (!$this->product_model->get_product_by_name($product_name))
		{
			echo 'Product not found!';
		} else {
			$product = $this->product_model->get_product_by_name($product_name);
			$data['product'] = $product;
			$this->load->view('searchsuccess', $data);
		}	
	}

	public function viewDetail($product_id)
	{
		$data['product'] = $this->product_model->get_product_by_id($product_id);
		$this->load->view('searchsuccess', $data);
	}
}