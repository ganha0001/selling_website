<?php
/**
 * Migration: CreateDatabase
 *
 * Created by: Cli for CodeIgniter <https://github.com/kenjis/codeigniter-cli>
 * Created on: 2016/11/02 07:26:59
 */
class Migration_CreateDatabase extends CI_Migration {

	public function up()
	{
		// CREATE USERS TABLE
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'password' => array(
				'type' =>'VARCHAR',
				'constraint' => 100
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');
		
		//// CREATE PRODUCTS TABLE

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			'product_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'price' => array(
				'type' =>'FLOAT',
				'constraint' => 11
			),
			'instock' => array(
				'type' =>'INT',
				'constraint' => 11
			),
			'image' => array(
				'type' =>'TEXT',
			),
			'description' => array(
				'type' =>'TEXT',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('products');

		//// CREATE ORDERS TABLE

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			'product_id' => array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'quantity' => array(
				'type' =>'INT',
				'constraint' => 11
			),
			'user_id' => array(
				'type' =>'INT',
				'constraint' => 11
			),
			'description' => array(
				'type' =>'TEXT',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('orders');

		// Adding a Column to a Table
		// $fields = array(
		// 	'preferences' => array('type' => 'TEXT')
		// );
		// $this->dbforge->add_column('table_name', $fields);
	}

	public function down()
	{
		// Dropping a table
		$this->dbforge->drop_table('users');
		$this->dbforge->drop_table('products');
		$this->dbforge->drop_table('orders');

//		// Dropping a Column From a Table
//		$this->dbforge->drop_column('table_name', 'column_to_drop');
	}

}
