<?php

class UserSeeder extends Seeder {

    public function run()
    {
        $this->db->truncate('users');

        $data = [
            'id' => 1,
            'username' => 'admin',
            'password' => '12345',
        ];
        $this->db->insert('users', $data);

        $data = [
            'id' => 2,
            'username' => 'root',
            'password' => '12345',
        ];
        $this->db->insert('users', $data);

        $data = [
            'id' => 3,
            'username' => 'user',
            'password' => '12345',
        ];
        $this->db->insert('users', $data);
    }

}