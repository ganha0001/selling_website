<?php

class ProductSeeder extends Seeder {

    public function run()
    {
        $this->db->truncate('products');

        $data = [
            'id' => 1,
            'product_name' => 'DabZ',
            'price' => 14.45,
            'instock' => 34,
            'image' => 'http://dummyimage.com/400x200.jpg/cc0000/ffffff',
            'description' => 'Sed ante. Vivamus tortor. Duis mattis egestas metus.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 2,
            'product_name' => 'Wordware',
            'price' => 10.37,
            'instock' => 18,
            'image' => 'http://dummyimage.com/400x200.jpg/5fa2dd/ffffff',
            'description' => 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 3,
            'product_name' => 'Jaxworks',
            'price' => 13.89,
            'instock' => 92,
            'image' => 'http://dummyimage.com/400x200.jpg/ff4444/ffffff',
            'description' => 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 4,
            'product_name' => 'Voomm',
            'price' => 10.15,
            'instock' => 99,
            'image' => 'http://dummyimage.com/400x200.jpg/ff4444/ffffff',
            'description' => 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 5,
            'product_name' => 'Trunyx',
            'price' => 13.56,
            'instock' => 86,
            'image' => 'http://dummyimage.com/400x200.jpg/ff4444/ffffff',
            'description' => 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 6,
            'product_name' => 'Thoughtbeat',
            'price' => 12.15,
            'instock' => 29,
            'image' => 'http://dummyimage.com/400x200.jpg/dddddd/000000',
            'description' => 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 7,
            'product_name' => 'Feedspan',
            'price' => 10.86,
            'instock' => 31,
            'image' => 'http://dummyimage.com/400x200.jpg/cc0000/ffffff',
            'description' => 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 8,
            'product_name' => 'Voomz',
            'price' => 16.40,
            'instock' => 73,
            'image' => 'http://dummyimage.com/400x200.jpg/dddddd/000000',
            'description' => 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.'
        ];

        $this->db->insert('products', $data);

        $data = [
            'id' => 9,
            'product_name' => 'Feedbug',
            'price' => 15.59,
            'instock' => 75,
            'image' => 'http://dummyimage.com/400x200.jpg/cc0000/ffffff',
            'description' => 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.'
        ];

        $this->db->insert('products', $data);
    }

}