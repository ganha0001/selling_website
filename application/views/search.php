<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>

<body>
	<form action="actionSearch" method="post">
		<h2>Searching product</h2>
		<input type="text" id="searchitem" name="searchitem" placeholder="Input product name: ">
		<button type="submit">Search</button>
	</form>
</body>
</html>