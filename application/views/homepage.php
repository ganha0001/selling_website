<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>
<body>
<div style="display: table;">
<h1 style="text-align: center;">My selling website</h1>
	<div style="margin-left: 50px">
		<div>
			<form action="product/actionSearch" method="post">
				<input type="text" id="searchitem" name="searchitem" placeholder="Search product: " required="">
				<button type="submit" style="margin-right: 20px">Search</button>
				<button type="button" onClick="window.location.href='user/login'" style="display: <?php echo check_logged() ? "none" : ""; ?>; margin-top: 20px">Login</button>
				<button type="button" onClick="window.location.href='user/register'" style="display: <?php echo check_logged() ? "none" : ""; ?>; margin-top: 20px">Register</button>
				<button type="button" onClick="window.location.href='order/history'" style="display: <?php echo check_logged() ? "" : "none"; ?>; margin-top: 20px">History</button>
				<button type="button" onClick="window.location.href='user/logout'" style="display: <?php echo check_logged() ? "" : "none"; ?>; margin-top: 20px">Logout</button>
			</form>
			
		</div>
		<div style="display: table; border-collapse: separate; border-spacing: : 30px">
			<?php 
				echo '<div style="display: table-row;">';
				$count = 0;
				foreach ($products as $key => $product)
				{
					if ($count == 2 || $count == 5 || $count == 8) 
					{
						
					}
					echo '<div style="display: table-cell; padding-right: 50px">
						<form action="order/actionOrder" method="post">
							<input type="hidden" value="' . $product->id . '" name="product_id">
							<h2 style="text-align: center;">' . strtoupper($product->product_name) .'</h2>
							<p>Price: $' . $product->price . '</p>
							<p>In stock: ' . $product->instock . ' </p>
							<a href="product/viewDetail/' . $product->id . '">
								<img src="' . $product->image . '" alt="">
							</a>
							<div><button type="submit">Buy now</button></div>
						</form>
					</div>';
					if ($count == 5 || $count == 8 || $count == 2) 
					{
						echo '</div>';
					}
					$count += 1;
				} 
				echo '</div>';
			?>
		</div>
	</div>
</div>
</body>
</html>