<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>
<body>
    <div id="root">
        <?php 
            foreach ($orders as $index => $order) {
                echo '<h1>'. ($index+1) .'</h1>
                <p>Customer username: '. $this->session->userdata("username") .'</p>
                <p>Product name: '. $this->product_model->get_product_by_id($order->product_id)->product_name .' </p>
                <p>Quantiry: '. $order->quantity .' </p>
                <p>Total price: '. $this->product_model->get_product_by_id($order->product_id)->price * $order->quantity .'</p>
                <p>Further information: '. $order->description;
            }
        ?>
    </div>
</body>
</html>