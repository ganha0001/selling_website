<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>

<body>
	<div>
		<form action="<?php echo base_url();?>order/actionOrder" method="post">
			<input type="hidden" value="<?php echo $product->id ?>" name="product_id">
			<div><h2><?php echo strtoupper($product->product_name); ?></h2></div>
			<img src="<?php echo $product->image; ?>" alt="">
			<p>Price: <?php echo $product->price ?></p>
			<p>In stock: <?php echo $product->instock ?></p>
			<p>Description: <?php echo $product->description ?> </p>
			<div><button type="submit">Buy now</button>
		</form>
		<button onclick="window.location.href='<?php echo base_url();?>'" type="button">Go back</button>	
	</div>
</body>
</html>