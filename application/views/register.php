<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>
<body>
    <div id="root">
        <form role="form" method="post" action="actionRegister">
            <h2>
                <span>Register</span>
            </h2>
            <div class="form-group">
                <input id="username" name="username" placeholder="Username" value="" required="">            
            </div>
            <div class="form-group">
                <input type="password" id="password" name="password" placeholder="Password" required="" style="margin-top: 5px">
            </div>
            <div class="form-group">
                <button type="submit" style="margin: 5px 0px 0px 10px">Enroll!</button>
                <button type="button" style="margin-left: 20px" onclick="window.location.href = '<?php echo base_url(); ?>'">Go back</button>
            </div>
        </form>
    </div>
</body>
</html>