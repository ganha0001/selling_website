<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to Selling Website</title>
</head>
<body>
    <div style="display: table-cell;">
        <form action="confirmOrder" method="post">
            <h1 style="text-align: center">Order</h1>
            <div>
                Name: 
                <input style="float: right" type="text" required="" name="name" value="<?php echo $this->session->userdata('username'); ?> ">
            </div>
            <div style="margin-top: 10px;">
                Address: 
                <input type="text" required="" name="address" placeholder="Address" style="float: right;">
            </div>
            <div style="margin-top: 10px;">
                Product: <?php echo $product->product_name; ?>
                <input type="hidden" name="productName" value="<?php echo $product->product_name; ?>">
            </div>
            <div style="margin-top: 10px">
                <img src="<?php echo $product->image; ?>" alt="" style="width: 240px;" >
            </div>
            <div style="margin-top: 10px;" id="totalPrice">
            <div id="priceText">Price: $<?php echo $product->price; ?></div>
                <input type="hidden" id="price" name="price" value="<?php echo $product->price; ?>">
            </div>
            <div style="margin-top: 10px;">
                Quantity: 
                <input type="number" name="quantity" id="quantity" value="1" onkeyup="changePrice()" onchange="changePrice()">
            </div>
            <div style="margin-top: 10px">
                <button type="submit" style="margin: 0px 50px 0px 20px">Submit</button>
                <button type="button" onclick="window.location.href = '<?php echo base_url(); ?>'">Cancel</button>
            </div>
        </form>
    </div>
</body>
<script type="text/javascript">
    function changePrice()
    {
        var priceDOM = document.getElementById('priceText');
        var inputPrice = document.getElementById('price');
        var inputQuantity = document.getElementById('quantity');
        if (inputQuantity.value < 0) inputQuantity.value = 0;
        var price = <?php echo $product->price; ?>;
        var totalPrice = (inputQuantity.value * price).toFixed(2);
        priceDOM.innerHTML = 'Price: $' + totalPrice;
        inputPrice.value = totalPrice;
    }
</script>
</html>

